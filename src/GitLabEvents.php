<?php

namespace Drupal\gitlab_api;

/**
 * Contains all events thrown by the gitlab_api module.
 */
final class GitLabEvents {

  public const CREATEPROJECT = 'create project';

}
